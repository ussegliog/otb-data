Lithology      Rhyolite
Sample#        AP-938-90
Chemistry
SiO2           77.87
Al2O3          11.88
Fe             1.55
CaO            0.10
TiO2           0.12
MnO            0.01
K2O            3.13
MgO            0.23
Na2O           3.84
P2O5           0.02
PPC            1.24
X-Ray Diffraction
               Quartz
               Plagioclase, Potassium Feldspar, Goethite
               Mica
               Chlorite
