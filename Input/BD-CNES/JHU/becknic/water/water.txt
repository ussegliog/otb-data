WATER

Only one spectrum (tapwater) of the four shown covers the entire spectral range 
from 0.4 to 14 micrometers.  The other spectra cover only the infrared, and are 
most useful for showing the close spectral similarity of all water samples, 
including foam, in the 2.08-14 micrometer region.  As pointed out in Salisbury 
and D'Aria (1992; 1994), the absorption coefficient of water is so high in the 
infrared beyond 3 micrometers that foam bubbles do not scatter light as they do 
at shorter wavelengths. 

REFERENCES

Salisbury, J. W. and D'Aria, D. M., 1992, Emissivity of terrestrial materials in the 
8-14 micormeter atmospheric window: Remote Sensing of Environ., v. 42, p. 83-
106.

Salisbury, J. W. and D'Aria, D. M., 1994, Emissivity of terrestrial materials in the 
3-5 micormeter atmospheric window: Remote Sensing of Environ., v. 47, p. 345-
361.
