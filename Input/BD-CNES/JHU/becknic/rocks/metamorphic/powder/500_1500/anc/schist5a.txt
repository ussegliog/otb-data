There are minor alkali feldspar anhedra and subhedra, many of which have been 
heavily sericitized, some muscovite and biotite (somewhat decomposed) with 
even less amphibole, which has possibly altered to talc in some regions, and a 
trace of opaques.  Modes were 58.7% quartz, 26.6% graphite, 8.2% mica. 0.9% 
amphibole and 0.5% opaques.

