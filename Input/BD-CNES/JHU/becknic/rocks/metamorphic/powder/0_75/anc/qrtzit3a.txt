There are trace amounts of zircon and lesser brown tourmaline (detrital), with a 
few scattered opaque/carbonaceous grains noted in this section.  The modal 
analyses for this rock gave 90.4% quartz, 6.96% muscovite, 1.74% opaques, and 
0.94% other. 
