Compostion:

Petrographic Description:  Very coarse-grained sample consisting of 
subhedral plagioclase (96%) with some areas showing mortar texture.  The 
plagioclase groundmass apparently is cryptocrystalline due to granulation.  
There are minute scattered amphibole anhedra (0.6%) and 0.7% opaques.  
There was no sign of calcite in thin section, nor was the rock analyzed for 
CO2, but the spectrum clearly indicates the presence of a small amount of 
carbonate.

Microprobe Analysis:  Microprobe analysis indicated a range of feldspar 
grains and groundmass composition from approx. An10 -An50 (oligoclase-
andesine); various cryptocrystalline amphiboles (either Tschermakite or 
hastingsite), opaques high in titanium but also high in silica and lime (a 
mixture), and possibly some zoisite.  Like most "anorthosites" of the 
Adirondacks, average plagioclase composition is andesine.  Hence, these 
rocks are sometimes classed with lime diorites instead of gabbros and 
referred to as "andesinites."

Chemical Analysis:
SiO2  53.41
CaO  9.65
TiO2   0.73
Na2O  5.31
Al2O3  25.79
K2O  1.16
Fe2O3  1.15
H2O  2.25
FeO  1.08
P2O5  0.04
MnO  0.02
MgO  0.25
TOTAL  100.84  (CO2 not analyzed)
