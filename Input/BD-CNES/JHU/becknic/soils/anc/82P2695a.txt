Moderate fine and medium platy structure.  Slightly hard, friable, plastic and 
sticky; strongly effervescent with an abrupt wavey boundary.  0.47% organic 
carbon, 16.4% clay, 41.7% silt, 41.9% sand.

Clay mineralogy: none.  

Coarse mineralogy: none.

Chemical analyses give 7% CaCO3 and 18% CaSO4.  
