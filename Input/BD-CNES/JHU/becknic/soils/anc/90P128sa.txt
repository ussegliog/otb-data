Weak fine granular structure, soft, friable and slightly sticky and plastic, 
neutral pH (7.2).  2.22% organic carbon, 22.8% clay, 54.6% silt, 22.6% 
sand.

Clay mineralogy: small mica, montmorillonite and kaolinite peaks and a 
weak quartz peak.

Coarse mineralogy:  71% quartz, 11% feldspar, 1% plagioclase feldspar, 1% 
potassic feldspar, 1% biotite, 4% glass, 1% plant opal, 1% iron oxides, 1% 
glass aggregate, 1% glass-coated grains, 2% other, 3% weathered aggregate, 
1% muscovite, and traces of hornblende, zircon, apatite, pyroxene, and 
garnet. 

