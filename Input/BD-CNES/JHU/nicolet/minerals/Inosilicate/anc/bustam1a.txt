Mineral:  Bustamite (Mn,Ca)3Si3O9

Sample No.: bustam.1

XRD Analysis:  Pure bustamite (Jim Crowley).

Chemistry:  Microprobe analysis showed grains selected to be 
homogeneous within and between grains.  Average of 12 analyses:

SiO2 	47.91
Al2O3	0.05
FeO 	0.31
MgO	0.20
CaO 	20.20
K2O	0.03
Na2O	0.11
TiO2	0.07
MnO 	31.73
Total 	100.58
