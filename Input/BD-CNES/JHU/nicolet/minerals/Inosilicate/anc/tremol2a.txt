Mineral:  Tremolite Ca2(Mg,Fe+2)5Si8O22(OH)2

Sample No.:  tremol.2

XRD Analysis:  Tremolite plus a trace of mica.

Chemistry:  Microprobe analysis showed the sample to be 
homogeneous within and between grains.  It has extremely high soda 
and potash for a tremolite.  Thus, chemically it is more like richterite 
than tremolite.  Average of 10 analyses:

SiO2	54.60
Al2O3	1.88
FeO	2.25
MgO	23.13
CaO	8.82
K2O	1.42
Na2O	5.54
TiO2	0.41
Cr2O3	0.19
Total	 *98.23
	*OH not measured.
