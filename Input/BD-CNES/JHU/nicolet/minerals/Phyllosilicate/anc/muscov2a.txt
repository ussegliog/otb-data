Mineral:  Muscovite KAl2(Si3Al)O10(OH,F)2

Sample No.: muscov.2

XRD Analysis  Pure muscovite.

Chemistry:  Volatile analysis by C. S. Papp, J. D. Sharkey, and K. J. 
Curry, and XRF analysis of the non-volatiles by J. Taggare, A. Bartel, 
and D. Diems, USGS Branch of Analytical Chemistry, Denver, showed:

SiO2	48.60
Al2O3	29.40
FeTO3 	5.80
MgO 	0.52
CaO	0.03
K2O	9.87
Na2O	0.40
TiO2	0.45
P2O5	<0.05
MnO	0.28
F	0.43
H2O	3.78
Other LOI	0.75
	_____
Total	100.36
