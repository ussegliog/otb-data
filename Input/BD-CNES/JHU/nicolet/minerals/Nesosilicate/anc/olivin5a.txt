Mineral:  Olivine (Fo51) (Fe+2,Mg)2SiO4

Sample No.:  olivine.5

XRD Analysis:  None.

Chemistry:  Microprobe analysis by W.I. Ridley, USGS, showed good 
Fo51 composition:

SiO2	34.34
TiO2	0.04
Al2O3	0.00
Cr2O3	0.04
MnO	0.73
NiO	0.07
FeO 	41.34
MgO 	23.80
CaO	0.05
	_____
Total	103.41
